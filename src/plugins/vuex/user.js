import axios from "./axios";

export default {
    actions:
        {
        fetchUserToken(context, data) {
            return new Promise((resolve, reject) => {
                axios
                    .post('http://localhost:1535/api/users/auth', data)
                    .then((response) => {
                        context.commit('updateToken', response.data)
                        resolve()
                    })
                    .catch(() => {
                        reject()
                    })
            })
        },
        pushDiyor(context, data) {
            return new Promise((resolve, reject) => {
                axios
                    .post('http://localhost:1535/api/users', data)
                    .then((response) => {
                        context.commit('updateDiyor', response.data.diyor)
                        resolve()
                    })
                    .catch(() => {
                        reject()
                    })
            })
        },
    },
    mutations: {
            updateToken(state, token) {
                state.token = token['accessToken']
                localStorage.setItem('access_token', token['accessToken'])
                localStorage.setItem('refresh_token', token['refreshToken'])
            },
            updateDiyor(state, diyor) {
                state.diyor = diyor
            },
        },


    state: {
        token: localStorage.getItem('access_token'),
        diyor: {
            email: '',
            password: ''
        }
    },
    getters: {
        getToken(state) {
            return state.token
        },
        getDiyor(state) {
            return state.diyor
        }
    }
}