import {createStore} from "vuex";
import user from "@/plugins/vuex/user";
import books from "@/plugins/vuex/book";
import category from "@/plugins/vuex/category";

export default createStore( {
    modules: {
        books,
        category,
        user,


    }
})