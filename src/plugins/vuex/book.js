import axios from "./axios";

export default {
    actions: {
        fetchBooks(context, categoryId = null) {

            let categoryUrl = ''
            if (categoryId !== null) {
                categoryUrl = '?category=' + categoryId
            }

            return new Promise( (resolve, reject) => {
                axios
                    .get('http://localhost:1535/api/books' + categoryUrl)
                    .then((response) => {
                        console.log('Kitoblar muvaffiqiyatli olind')

                        let books = {
                            models: response.data['hydra:member'],
                            totalItems: response.data['hydra:totalItems'],

                        }

                        context.commit('updateBooks', books)
                        resolve()
                    })
                    .catch(() => {
                        console.log('Kitoblar olishda xatolik yuzberdi.Books.jsni Tekshiring')
                        reject()
                    })
            })
        },
        fetchBook(context, bookId ) {
            return new Promise( (resolve, reject) => {
                axios
                    .get('http://localhost:1535/api/books/' + bookId)
                    .then((response) => {
                        context.commit('updateBook', response.data)
                        resolve()
                    })
                    .catch(() => {
                        console.log('Kitoblar olishda xatolik')
                        reject()
                    })
            })
        },
        pushKitob(context,data){
            return new Promise((resolve, reject) => {
                axios
                    .post('http://localhost:1535/api/books', data)
                    .then((response) => {
                        console.log('Kitob yaratildi')
                        context.commit('updateKitob',response.data.kitob)
                        resolve()
                    })
                    .catch(() => {
                        console.log('Xatolik Kitob yaratilmadi')
                        reject()
                    })
            })
        }
    },
    mutations: {
        updateBooks(state, books) {
            state.books = books
        },
        updateBook(state, book) {
            state.book = book
        },
        updateKitob(state, kitob){
            state.kitob = kitob
        }

    },
    state: {
        books: {
            models: [],
            totalItems: 0
        },
        book: {
            name: '',
            text: ''
        },
        kitob: {
            name: '',
            description: '',
            text: '',
            category: ''
        }
    },
    getters: {
        getBooks(state) {
            return state.books.models
        },
        getBook(state) {
            return state.book
        },
        getKitob(state) {
            return state.kitob
        }
    }
}